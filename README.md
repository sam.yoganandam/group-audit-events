# Group Audit Events

Delivers complete audit events for a group, including subgroups and projects. Provides CSV and JSON representations as well as a filterable HTML table.

The standard GitLab audit events are currently limited to individual groups and projects. On GitLab.com, you probably want to see everything that is happening in your group. This tool facilitates this.

## Usage

### Pages

* Fork this project
* Make sure to set it to **confidential**
* Modify `.gitlab-cs.yml` to configure your Group as a variable
* Add a CI/CD variable `GIT_TOKEN` containing your API token
* Run the pipeline to get your report
* Upvote this issue https://gitlab.com/gitlab-org/gitlab/-/issues/214426

### Data crawler

To get the audit events data including JSON and CSV representation, run the python script:

`pip3 install -r requirements.txt`
`python3 group-audit-log.py $GIT_TOKEN $GROUP_ID`

$GIT_TOKEN needs to be able to read the whole group. A group owner API token is ideal.

$GROUP_ID must be the ID of the group for which this report should run. You can find the group ID under the name of the group in the Group overview.

### HTML Frontend

For the nice filterable HTML representation:

`yarn install --frozen-lockfile`
`yarn dev` (to run locally) OR
`yarn build` (to run with GitLab pages)

## Disclaimer

This script is provided for educational purposes. It is not supported by GitLab. However, you can create an issue if you need help or better propose a Merge Request. This script collects audit events which may hold confidential business and/or user data. Please use this script and its outputs with caution and in accordance to your local data privacy regulations.