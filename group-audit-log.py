#!/usr/bin/env python3

import gitlab
import argparse
import requests
import json
import csv

class Audit_Log_Compiler():

    gitlab_url = "https://gitlab.com"
    api_url = "https://gitlab.com/api/v4/"
    group = None
    token = None
    headers = None
    groups = []
    projects = []
    audit_events = []

    def __init__(self, args):
        if args.gitlab:
            self.gitlab_url = args.gitlab if args.gitlab.endswith("/") else args.gitlab + "/"
            self.api_url = self.gitlab_url + "api/v4/"
        self.token = args.token
        self.group = args.group
        self.headers = {'PRIVATE-TOKEN': args.token}
        self.get_subgroups(self.group)
        self.get_projects(self.group)

    def get_subgroups(self, group):
        print("Getting subgroups for group %s" % group)
        gl = gitlab.Gitlab(self.gitlab_url, private_token=self.token)
        group_object = gl.groups.get(group)
        self.groups.append(group_object)

        subgroups = group_object.subgroups.list()
        for subgroup in subgroups:
            subgroup_object = gl.groups.get(subgroup.id)
            self.get_subgroups(subgroup_object.id)

    def get_projects(self, group):
        print("Getting projects for group %s" % group)
        gl = gitlab.Gitlab(self.gitlab_url, private_token=self.token)
        group_object = gl.groups.get(group)
        group_projects = group_object.projects.list(include_subgroups=True)
        for project in group_projects:
            self.projects.append(gl.projects.get(project.id))

    def get_audit_logs(self, groups, projects):
        event_logs = []
        for group in groups:
            event_logs.extend(self.get_audit_events(group, "groups"))
        for project in projects:
            event_logs.extend(self.get_audit_events(project, "projects"))
        return event_logs

    def get_audit_events(self, resource, resource_type):
        path = "/%s/%s/audit_events" % (resource_type, resource.id)
        page = 1
        event_log = []
        while True:
            events = self.request_resource(path, page)
            if events:
                event_log.extend(events)
                if len(events) < 100:
                    break
                page += 1
            else:
                print("Can't get resource: %s page %s" % (path, page))
        return event_log

    def request_resource(self, path, page):
        default_params = { "page":page, "per_page":100}
        request_url = self.api_url + path
        data = requests.get(request_url, params=default_params, headers=self.headers)
        return data.json()

    def flatten(self, event_logs):
        flat_logs = []
        #fields = ["author", "object", "object_type","action_type","action","target_type","target_details","IP","date"]
        for event in event_logs:
            if not isinstance(event, dict):
                continue
            flat_event = {}
            flat_event["author"] = event["details"]["author_name"]
            flat_event["object"] = event["details"]["entity_path"]
            flat_event["object_type"] = event["entity_type"]
            if "add" in event["details"]:
                flat_event["action_type"] = "ADD"
                flat_event["action"] = event["details"]["add"]
                if "as" in event["details"]:
                    flat_event["action"] += " as " + event["details"]["as"]
            elif "remove" in event["details"]:
                flat_event["action_type"] = "REMOVE"
                flat_event["action"] = event["details"]["remove"]
            elif "change" in event["details"]:
                flat_event["action_type"] = "CHANGE"
                flat_event["action"] = "%s from %s to %s" % (event["details"]["change"],event["details"]["from"],event["details"]["to"])
            else:
                flat_event["action_type"] = "OTHER"
                if "custom_message" in event["details"]:
                    flat_event["action"] = event["details"]["custom_message"]
                else:
                    flat_event["action"] = ""
            flat_event["target_type"] = event["details"]["target_type"]
            flat_event["target_details"] = event["details"]["target_details"]
            flat_event["IP"] = event["details"]["ip_address"]
            flat_event["date"] = event["created_at"]
            flat_logs.append(flat_event)
        return flat_logs

    def write_csv(self, events, file):
        with open(file, "w") as outfile:
            fields = ["author", "object", "object_type","action_type","action","target_details","target_type","IP","date"]
            reportwriter = csv.writer(outfile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            reportwriter.writerow(fields)
            for event in events:
                row = []
                for field in fields:
                    if field in event:
                        row.append(event[field])
                    else:
                        row.append("")
                reportwriter.writerow(row)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create complete group audit report (including projects)')
    parser.add_argument('token', help='API token able to read the requested group')
    parser.add_argument('group', help='ID of the group to compile the audit log for')
    parser.add_argument('--gitlab', help='GitLab URL, defaults to https://gitlab.com/')
    args = parser.parse_args()
    compiler = Audit_Log_Compiler(args)
    audit_log = compiler.get_audit_logs(compiler.groups, compiler.projects)
    flat_log = compiler.flatten(audit_log)
    outfile = "group_audit_log.csv"
    compiler.write_csv(flat_log, outfile)
    # need to parse the audit log to resolve entity, author and target ids

    with open("group_audit_log.json", "w") as logfile:
        json.dump(flat_log, logfile, indent=4, ensure_ascii=False)
